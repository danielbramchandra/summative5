import React, { Component } from 'react'

class NavigationFooter extends Component {
    render() {
        return (
            <ul class="navigation">
                <li>
                    <a href="">Home</a>
                </li>
                <li>
                    <a href="">Tickets</a>
                </li>
                <li>
                    <a href="">The Zoo</a>
                </li>
                <li>
                    <a href="">Events</a>
                </li>
                <li>
                    <a href="">Blog</a>
                </li>
                <li>
                    <a href="">Gallery</a>
                </li>
            </ul>
        )
    }
}
export default NavigationFooter;