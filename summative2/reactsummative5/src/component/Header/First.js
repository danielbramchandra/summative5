import React, { Component } from 'react'

class First extends Component {
    render() {
        return (
            <ul>
                <li>
                    <h2>
                        <a href="">Live</a>
                    </h2>
                    <span>Have fun in your visit</span>
                </li>
                <li>
                    <h2>
                        <a href="">Love</a>
                    </h2>
                    <span>Donate for the animal</span>
                </li>
                <li>
                    <h2>
                        <a href="">Learn</a>
                    </h2>
                    <span>Get to know the animal</span>
                </li>
            </ul>
        )
    }
}
export default First;