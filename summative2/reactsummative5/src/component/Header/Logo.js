import React, { Component } from 'react'

class Logo extends Component {
    render() {
        return (
            <a id="logo" href="">
                <img src="./images/logo.jpg" alt=""></img>
            </a>
        )
    }
}
export default Logo;