import React, { Component } from 'react'

class Navigation extends Component {
    render() {
        return (
            <ul id="navigation">
                <li id="link1" class="selected">
                    <a href="">Home</a>
                </li>
                <li id="link2">
                    <a href="">The Zoo</a>
                </li>
                <li id="link3">
                    <a href="">Visitor Info</a>
                </li>
                <li id="link4">
                    <a href="">Ticket</a>
                </li>
                <li id="link5">
                    <a href="">Event</a>
                </li>
                <li id="link6">
                    <a href="">Gallery</a>
                </li>
                <li id="link7">
                    <a href="">Contact Us</a>
                </li>
            </ul>
        )
    }
}
export default Navigation;