import React, { Component } from 'react'

class EventData extends Component {
    constructor(props) {
        super(props)
        this.state = {
            month: "",
            day: ""
        }
    }
    componentDidMount() {

        let date = new Date(this.props.date)
        this.setState({ month: date.toLocaleString('default', { month: 'short' }), day: date.getDate() })
        console.log()
    }
    render() {
        return (
            <li class="first"><a href="">
                <span>{this.state.month} {this.state.day}</span>
                <p>{this.props.description}</p>
            </a>
            </li>
        )
    }
}
export default EventData;