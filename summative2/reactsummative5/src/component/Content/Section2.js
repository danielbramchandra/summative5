import React, { Component } from 'react'
import BlogHero from './BlogHero';
import BlogSection from './BlogSection';

class Section2 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            blog: []
        }
    }
    componentDidMount() {
        fetch("http://localhost:8080/blog/getall").then((res) =>
            res.json()
        )
            .then((json) => {
                this.setState({
                    blog: json,
                });
            }
            );
    }
    render() {
        return (
            <div class="section2">
                <h2>Blog : </h2>
                <p>Blog</p>
                <BlogHero />
                <div id="section1">
                    {
                        this.state.blog.slice(0, 2).map((data) => {
                            return (<BlogSection imgUrl={data.imgUrl} title={data.title} description={data.description} />)
                        })
                    }
                </div>
                <div id="section2">
                    {
                        this.state.blog.slice(-1).map((data) => {
                            return (<BlogSection imgUrl={data.imgUrl} title={data.title} description={data.description} />)
                        })
                    }
                    <BlogSection />
                    {/* <ul>
                        <li>
                            <img src="./images/butterfly-2.jpg" alt=""></img>
                            <h4>This website tempalte has been</h4>
                            <p>design by <a href="">Nexsoft Bootcamp</a> for you, for free</p>
                        </li>
                    </ul> */}
                </div>

            </div>
        )
    }
}
export default Section2