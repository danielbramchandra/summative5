import React, { Component } from 'react'
import ImageGalleryData from './ImageGalleryData';


class ImageGallery extends Component {
    constructor(props) {
        super(props)
        this.state = {
            animal: []
        }
    }
    componentDidMount() {
        fetch("http://localhost:8080/animal/getall").then((res) =>
            res.json()
        )
            .then((json) => {
                this.setState({
                    animal: json,
                });
            }
            );

    }
    render() {
        return (
            <ul>
                {
                    this.state.animal.map((data) => {
                        return (<ImageGalleryData imgUrl={data.img} name={data.name} />)
                    })
                }
                <li class="last"><a href=""><img src="./images/button-view-gallery.jpg" alt=""></img></a>Gallery</li>
            </ul>
        )
    }
}
export default ImageGallery