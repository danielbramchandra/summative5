import React, { Component } from 'react'

class BlogSection extends Component {
    render() {
        return (
            <ul>
                <li>
                    <img src={this.props.imgUrl} alt=""></img>
                    <h4>{this.props.title}</h4>
                    <p>{this.props.description}</p>
                </li>
            </ul>
        )
    }
}
export default BlogSection