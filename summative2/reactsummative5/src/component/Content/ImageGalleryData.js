import React, { Component } from 'react'

class ImageGalleryData extends Component {
    render() {
        return (
            <li className={this.props.className}>
                <a href="">
                    <img src={this.props.imgUrl} alt=""></img>
                </a>{this.props.name}
            </li>
        )
    }
}
export default ImageGalleryData