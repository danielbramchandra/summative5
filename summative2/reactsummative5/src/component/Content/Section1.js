import React, { Component } from 'react'
import EventData from './EventData';
class Section1 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            eventData: [],
        }

    }

    componentDidMount() {
        fetch("http://localhost:8080/event/getall").then((res) =>
            res.json()
        )
            .then((json) => {
                this.setState({
                    eventData: json,
                });
            }
            );


    }
    render() {

        return (
            <div class="section1">
                <h2>Events</h2>
                <ul id="article">
                    {
                        this.state.eventData.map((data) => {
                            return (
                                <EventData date={data.date} description={data.description} />
                            )

                        })
                    }
                </ul>
            </div>
        )
    }
}
export default Section1;