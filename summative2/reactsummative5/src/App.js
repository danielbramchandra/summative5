

import Content from './pages/Content';
import Footer from './pages/Footer';
import Header from './pages/Header';

function App() {
  return (
    <div id="page">
      <Header />
      <Content />
      <Footer />
    </div>
  );
}

export default App;
