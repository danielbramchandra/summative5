import React, { Component } from 'react'
import First from '../component/Header/First';
import Logo from '../component/Header/Logo';
import Navigation from '../component/Header/Navigation';

class Header extends Component {
    render() {
        return (
            <header id="header">
                {/* LOGO COMPONENT */}
                <Logo />
                {/* FIRST COMPONENT */}
                <First />
                <a href="">Buy Ticket / Check Events</a>
                {/* NAVIGATION COMPONENT */}
                <Navigation />
                <img src="./images/lion-family.jpg" alt=""></img>
                <div>
                    <h1>Special Events</h1>
                    <p>This website template was design by <a href="">Nexsoft Bootcamp</a></p>
                </div>

            </header>
        )
    }
}
export default Header;