import React, { Component } from 'react'
import ImageGallery from '../component/Content/ImageGallery'
import Section1 from '../component/Content/Section1'
import Section2 from '../component/Content/Section2'
import Section3 from '../component/Content/Section3'

class Content extends Component {
    render() {
        return (
            <div id="content">
                <div id="featured">
                    <h2>Meet Our Animals</h2>
                    <ImageGallery />
                </div>
                <Section1 />
                <Section2 />
                <Section3 />
            </div>
        )
    }
}
export default Content