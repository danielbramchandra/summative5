import React, { Component } from 'react'
import InfoData from '../component/Footer/InfoData';
import LogoFooter from '../component/Footer/LogoFooter';
import NavigationFooter from '../component/Footer/NavigationFooter';
import Navigation from '../component/Header/Navigation';

class Footer extends Component {
    render() {
        return (
            <footer id="footer">
                <div>
                    <LogoFooter />
                    <InfoData />
                    {/* <Navigation /> */}
                    <NavigationFooter />

                    <ul>
                        <li><a href="">Live : Have fun in your visit</a></li>
                        <li><a href="">Love : Donate for the Animals</a></li>
                        <li><a href="">Learn : Get to know the Animals</a></li>
                    </ul>
                    <p>Copyright &copy 2011. All Rights Reserved</p>
                </div>
            </footer>
        )
    }
}
export default Footer;