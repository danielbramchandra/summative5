package id.co.nexsoft.restapisummative5.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.restapisummative5.model.BlogData;

public interface BlogRepository extends CrudRepository<BlogData, Integer> {
    @Query("SELECT u FROM blog_ u ORDER BY DESC LIMIT 4;")
    ArrayList<BlogData> findNewestBlog();
}
