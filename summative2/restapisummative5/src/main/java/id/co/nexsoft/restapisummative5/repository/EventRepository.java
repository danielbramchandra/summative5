package id.co.nexsoft.restapisummative5.repository;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.restapisummative5.model.EventData;

public interface EventRepository extends CrudRepository<EventData, Integer> {

}
