package id.co.nexsoft.restapisummative5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Restapisummative5Application {

	public static void main(String[] args) {
		SpringApplication.run(Restapisummative5Application.class, args);
	}

}
