package id.co.nexsoft.restapisummative5.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.restapisummative5.model.BlogData;
import id.co.nexsoft.restapisummative5.repository.BlogRepository;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/blog")
public class BlogDataController {
    @Autowired
    BlogRepository repo;

    @GetMapping(value = "/getall")
    public ArrayList<BlogData> getall() {
        return (ArrayList<BlogData>) repo.findAll();
    }

    @GetMapping(value = "/getNewestData")
    public ArrayList<BlogData> getNewest() {
        return (ArrayList<BlogData>) repo.findNewestBlog();
    }
}
