package id.co.nexsoft.restapisummative5.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.restapisummative5.model.EventData;
import id.co.nexsoft.restapisummative5.repository.EventRepository;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/event")
public class EventDataController {
    @Autowired
    EventRepository repo;

    @GetMapping(value = "/getall")
    public ArrayList<EventData> getall() {
        return (ArrayList<EventData>) repo.findAll();
    }
}
