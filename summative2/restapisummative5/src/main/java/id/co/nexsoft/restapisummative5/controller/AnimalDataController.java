package id.co.nexsoft.restapisummative5.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.restapisummative5.model.AnimalData;
import id.co.nexsoft.restapisummative5.repository.AnimalRepository;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/animal")
public class AnimalDataController {
    @Autowired
    AnimalRepository repo;

    @GetMapping(value = "/getall")
    public ArrayList<AnimalData> getall() {
        return (ArrayList<AnimalData>) repo.findAll();
    }
}
