package id.co.nexsoft.restapisummative5.repository;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.restapisummative5.model.AnimalData;

public interface AnimalRepository extends CrudRepository<AnimalData, Integer> {
    
}
